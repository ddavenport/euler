"""Usage: python problem6.py

Solves problem 6 of Project Eurler
Find the difference between the sum of the squares of the
first one hundred natural numbers and the square of the sum

Project URL: https://projecteuler.net/problem=6
"""

print((sum(range(101))**2) - sum([x**2 for x in range(101)]))
