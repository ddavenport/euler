"""Usage: python problem16.py

Solves problem 16 of Project Eurler
What is the sum of the digits of the number 21000

Project URL: https://projecteuler.net/problem=16
"""

print(sum(map(int, str(2 ** 1000))))
