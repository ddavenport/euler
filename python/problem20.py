"""Usage: python problem20.py

Solves problem 20 of Project Eurler
Find the sum of the digits in the number 100!

Project URL: https://projecteuler.net/problem=20
"""

import functools
print(sum(map(int, str(functools.reduce(lambda x,y: x*y, range(100, 0, -1))))))
