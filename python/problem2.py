"""Usage: python problem2.py

Solves problem 2 of Project Eurler
By considering the terms in the Fibonacci sequence whose values do not exceed four million,
find the sum of the even-valued terms

Project URL: https://projecteuler.net/problem=2
"""

def fib (n):
    a = 1
    b = 2
    for x in range(n):
        yield(a)
        tmp = a + b
        a = b
        b = tmp

print(sum([x for x in fib(5) if x % 2 == 0]))
