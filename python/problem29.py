"""Usage: python problem29.py

Solves problem 29 of Project Eurler
How many distinct terms are in the sequence generated
by ab for 2 ≤ a ≤ 100 and 2 ≤ b ≤ 100?

Project URL: https://projecteuler.net/problem=29
"""

print(len(set([x**y for y in range(2,101) for x in range(2,101)])))
