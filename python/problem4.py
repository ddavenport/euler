"""Usage: python problem4.py

Solves problem 4 of Project Eurler
Find the largest palindrome made from the product of two 3-digit numbers

Project URL: https://projecteuler.net/problem=4
"""

def is_palindrome(n):
    """checks if number is a palindrome"""
    n = str(n)
    if n == n[::-1]:
        return True
    else:
        return False


largest = max(filter(is_palindrome, [x * y for y in range(100, 1000) for x in range(100, 1000)]))

print(largest)
